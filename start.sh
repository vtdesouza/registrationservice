#!/bin/sh

echo Building with Maven
mvn clean package

echo Starting Jetty server
java -jar target/server-0.0.1-SNAPSHOT.jar
