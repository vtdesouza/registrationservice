package registrationservice.business;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.regex.Pattern;

import registrationservice.business.impl.DefaultExclusionService;
import registrationservice.dto.UserRegistration;
import registrationservice.exceptions.InvalidUserDataException;
import registrationservice.exceptions.UserAlreadExistException;
import registrationservice.exceptions.UserExcludedException;
import registrationservice.persistence.UserDAO;

/**
 * Class responsible for the user registration's business logic.
 * @author Vitor Couto
 *
 */
public class RegistrationService {
	
	private static Pattern passwordPattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$");
	private static Pattern usernamePattern = Pattern.compile("^([0-9a-zA-Z]{1,})$");
	private static Pattern ssnPattern = Pattern.compile("^[0-9]{1,}$"); // Only checking if it is numerical (dumb check)
	private ExclusionService exclusionService;
	private UserDAO userDAO;
	
	public RegistrationService() {
		exclusionService = new DefaultExclusionService();
		this.userDAO = new UserDAO();
	}
	
	public RegistrationService(ExclusionService exclusionService, UserDAO userDAO) {
		this.exclusionService = exclusionService;
		this.userDAO = userDAO;
	}
	
	/**
	 * Registrate a new user.
	 * @param user the new user's data
	 * @throws UserExcludedException
	 * @throws InvalidUserDataException
	 * @throws UserAlreadExistException
	 */
	public void registrate(UserRegistration user) {
		validateUserData(user);
		checkIfExcluded(user);
		checkIfExists(user);
		userDAO.register(user);
	}
	
	/**
	 * Check if an user is inscribed into the exclusion list.
	 * @param user the user's data
	 * @throws UserExcludedException
	 */
	private void checkIfExcluded(UserRegistration user) {
		if (!exclusionService.validate(user.getDateOfBirth(), user.getSsn())) {
			throw new UserExcludedException("Excluded User: SSN = " + user.getSsn() + " | birth = " + user.getDateOfBirth());
		}
	}
	
	/**
	 * Validate the user's data.
	 * @param user the user's data
	 * @throws UserExcludedException
	 * @throws InvalidUserDataException
	 * @throws UserAlreadExistException
	 */
	private void validateUserData(UserRegistration user) {
		validateUserName(user.getUserName().toLowerCase());
		validatePassword(user.getPassword());
		validateDateOfBirth(user.getDateOfBirth());
		validateSSN(user.getSsn());
	}
	
	/**
	 * Check if SSN is valid.
	 * @param ssn the SSN to be checked
	 * @throws InvalidUserDataException
	 */
	private void validateSSN(String ssn) {
		validatePatern(ssnPattern, ssn, "Invalid SSN");
	}

	/**
	 * Check if the date of birth is valid (ISO-8601).
	 * @param dateOfBirth date to be checked
	 * @throws InvalidUserDataException
	 */
	private void validateDateOfBirth(String dateOfBirth) {
		try {
			LocalDate.parse(dateOfBirth);		
		} catch (DateTimeParseException e) {
			throw new InvalidUserDataException("Date of birth is not valid");
		}
	}

	/**
	 * Check if the password is valid.
	 * @param password password to be checked
	 * @throws InvalidUserDataException
	 */
	private void validatePassword(String password) {
	    validatePatern(passwordPattern, password, "Password must have at least four characters, at least one lower case character, at least one upper case character, at least one number");
	}

	/**
	 * Check if the user name is valid.
	 * @param userName user name to be checked
	 * @throws InvalidUserDataException
	 */
	private void validateUserName(String userName) {
		validatePatern(usernamePattern, userName, "Username must be alphanumerical with no spaces");
	}

	/**
	 * Check if the user is already registered.
	 * @param user user to be checked
	 * @throws UserAlreadExistException
	 */
	private void checkIfExists(UserRegistration user) {
		if (userDAO.exist(user)) {
			throw new UserAlreadExistException("User already registered: SSN = " + user.getSsn());
		}
	}
	
	/**
	 * Check if a String is in compliance to a given REGEX.
	 * @param pattern REGEX to defining a accepted value
	 * @param value String to be checked
	 * @param errorMessage information message in case of error
	 * @throws InvalidUserDataException
	 */
	private void validatePatern(Pattern pattern, String value, String errorMessage) {
		if (!pattern.matcher(value).matches()) {
	    	throw new InvalidUserDataException(errorMessage);
	    }
	}

}
