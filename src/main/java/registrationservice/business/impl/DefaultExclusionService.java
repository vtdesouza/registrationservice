package registrationservice.business.impl;

import java.util.ArrayList;
import java.util.List;

import registrationservice.business.ExclusionService;

/**
 * Default implementation for the ExclusionService. 
 * @author Vitor Couto
 *
 */
public class DefaultExclusionService implements ExclusionService {

	private static List<String> exclusionList = new ArrayList<>();
	
	{
		exclusionList.add("853850751815-12-10");
		exclusionList.add("1234567891912-06-23");
		exclusionList.add("9876543211910-06-22");
	}
	
	@Override
	public boolean validate(String dateOfBirth, String ssn) {
		return !exclusionList.contains(ssn+dateOfBirth);
	}

}
