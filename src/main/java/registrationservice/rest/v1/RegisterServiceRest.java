package registrationservice.rest.v1;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import registrationservice.business.RegistrationService;
import registrationservice.dto.UserRegistration;
import registrationservice.exceptions.InvalidUserDataException;
import registrationservice.exceptions.UserAlreadExistException;
import registrationservice.exceptions.UserExcludedException;

/**
 * Defines the REST interface for the Register service. 
 * @author Vitor Couto
 *
 */
@Path("v1/register")
public class RegisterServiceRest {

	private RegistrationService service = new RegistrationService();
	
	/**
	 * POST service to register an user.
	 * @param user the request data for the user to be registered
	 * @return Rest response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(UserDataRequest user) {
		try {
			service.registrate(new UserRegistration(user.getUserName(), user.getPassword(), user.getDateOfBirth(), user.getSsn()));
			return Response.created(UriBuilder.fromResource(RegisterServiceRest.class).path(user.getSsn()).build()).build();
		} catch (InvalidUserDataException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (UserAlreadExistException e) {
			return Response.status(422).entity(e.getMessage()).build();
		} catch (UserExcludedException e) {
			return Response.status(Status.FORBIDDEN).entity(e.getMessage()).build();
		}
	}
}
