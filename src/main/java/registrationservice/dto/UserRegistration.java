package registrationservice.dto;

/**
 * DTO containing all the user's registration data
 * @author Vitor Couto
 *
 */
public class UserRegistration {

	private final String userName;
	private final String password;
	private final String dateOfBirth;
	private final String ssn;
	
	public UserRegistration(String userName, String password, String dateOfBirth, String ssn) {
		super();
		this.userName = userName;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
		this.ssn = ssn;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public String getSsn() {
		return ssn;
	}
}
