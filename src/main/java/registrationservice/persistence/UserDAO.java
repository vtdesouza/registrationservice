package registrationservice.persistence;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import registrationservice.dto.UserRegistration;

/**
 * DAO responsible for the persistence of an User data.
 * @author Vitor Couto
 *
 */
public class UserDAO {

	private static Set<String> registered = new HashSet<>(Arrays.asList("111", "222", "333"));
	
	/**
	 * Check if an user is already registered.
	 * @param user user to be checked
	 * @return True if already registered or false otherwise
	 */
	public boolean exist(UserRegistration user) {
		return registered.contains(user.getSsn());
	}
	
	/**
	 * Add an user into the registered users database. 
	 * @param user user to be added
	 */
	public void register(UserRegistration user) {
		registered.add(user.getSsn());
	}
}
