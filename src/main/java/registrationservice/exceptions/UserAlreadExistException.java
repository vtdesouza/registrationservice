package registrationservice.exceptions;

/**
 * Exception for an user that is already registered in the system.
 * @author Vitor Couto
 *
 */
public class UserAlreadExistException extends RuntimeException {

	private static final long serialVersionUID = -5182244873293532639L;

	public UserAlreadExistException() {
	}

	public UserAlreadExistException(String message) {
		super(message);
	}

	public UserAlreadExistException(Throwable cause) {
		super(cause);
	}

	public UserAlreadExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserAlreadExistException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
