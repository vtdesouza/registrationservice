package registrationservice.exceptions;

/**
 * Exception representing that some of the provided user data is invalid.
 * @author Vitor Couto
 *
 */
public class InvalidUserDataException extends RuntimeException {

	private static final long serialVersionUID = 7363045196735990830L;

	public InvalidUserDataException() {
	}

	public InvalidUserDataException(String message) {
		super(message);
	}

	public InvalidUserDataException(Throwable cause) {
		super(cause);
	}

	public InvalidUserDataException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidUserDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
