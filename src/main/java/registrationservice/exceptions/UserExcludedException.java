package registrationservice.exceptions;

/**
 * Exception for an user that is inscribed into the exclusion list.
 * @author Vitor Couto
 *
 */
public class UserExcludedException extends RuntimeException {

	private static final long serialVersionUID = -8533546248589696389L;

	public UserExcludedException() {
	}

	public UserExcludedException(String message) {
		super(message);
	}

	public UserExcludedException(Throwable cause) {
		super(cause);
	}

	public UserExcludedException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserExcludedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}