package registrationservice.business;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import registrationservice.mock.ExclusionServiceMock;
import registrationservice.business.ExclusionService;
import registrationservice.business.RegistrationService;
import registrationservice.dto.UserRegistration;
import registrationservice.exceptions.InvalidUserDataException;
import registrationservice.exceptions.UserAlreadExistException;
import registrationservice.exceptions.UserExcludedException;
import registrationservice.persistence.UserDAO;

public class RegistrationServiceTest {

	private RegistrationService service;
	private ExclusionService exclusionService;
	
	private String validName;
	private String invalidName;

	private String validPassword;
	private List<String> invalidPasswords;
	
	private String validDate;
	private List<String> invalidDates;
	
	private String validSSN;
	private List<String> invalidSSNs;
	private String registeredSSN;
	
	
	@Before
	public void setUp() throws Exception {
		validName = "vt1984";
		invalidName = "vt 1984";
		               
		validPassword = "aA123";
		invalidPasswords = Arrays.asList("", "a1A", "aaaa12", "AAA12", "12345", "aA12 3");
		               
		validDate = "1950-04-19";
		invalidDates = Arrays.asList("1999-02-29", "2000-15-01", "2000-10-00", "20001023", "2000-10-AA", "10-03-0000");
		               
		validSSN = "097845617";
		invalidSSNs = Arrays.asList("", "0000000a", "123 4", "28-99");
		registeredSSN = "111";
		
		exclusionService = new ExclusionServiceMock();
		
		service = new RegistrationService(exclusionService, new UserDAO());
	}

	@Test(expected=UserAlreadExistException.class)
	public void testRegistrateOK() {
		UserRegistration user = new UserRegistration(validName, validPassword, validDate, validSSN);
		service.registrate(user);
		service.registrate(user);
	}
	
	@Test(expected=InvalidUserDataException.class)
	public void testRegistrateInvalidName() {
		UserRegistration user = new UserRegistration(invalidName, validPassword, validDate, validSSN);
		service.registrate(user);
	}
	
	@Test
	public void testRegistrateInvalidPasswords() {
		for (String invalidPassword : invalidPasswords) {
			boolean exceptionThrown = false;
			UserRegistration user = new UserRegistration(validName, invalidPassword, validDate, validSSN);
			try {
				service.registrate(user);				
			} catch (InvalidUserDataException e) {
				exceptionThrown = true;
				assertEquals("Password must have at least four characters, at least one lower case character, at least one upper case character, at least one number", e.getMessage());
			}
			assertTrue("Exception should have been throw for password " + invalidPassword, exceptionThrown);
			
		}
	} 
	
	@Test
	public void testRegistrateInvalidDate() {
		for (String invalidDate : invalidDates) {
			boolean exceptionThrown = false;
			UserRegistration user = new UserRegistration(validName, validPassword, invalidDate, validSSN);
			try {
				service.registrate(user);				
			} catch (InvalidUserDataException e) {
				exceptionThrown = true;
				assertEquals("Date of birth is not valid", e.getMessage());
			}
			assertTrue("Exception should have been throw for date " + invalidDate, exceptionThrown);
			
		}
	}
	
	@Test
	public void testRegistrateInvalidSSN() {
		for (String invalidSSN : invalidSSNs) {
			boolean exceptionThrown = false;
			UserRegistration user = new UserRegistration(validName, validPassword, validDate, invalidSSN);
			try {
				service.registrate(user);				
			} catch (InvalidUserDataException e) {
				exceptionThrown = true;
				assertEquals("Invalid SSN", e.getMessage());
			}
			assertTrue("Exception should have been throw for SSN " + invalidSSN, exceptionThrown);
			
		}
	}
	
	@Test(expected=UserAlreadExistException.class)
	public void testRegistrateAlreadyExist() {
		UserRegistration user = new UserRegistration(validName, validPassword, validDate, registeredSSN);
		service.registrate(user);
	}
	
	@Test(expected=UserExcludedException.class)
	public void testRegistrateExcluded() {
		((ExclusionServiceMock)exclusionService).valid = false;
		UserRegistration user = new UserRegistration(validName, validPassword, validDate, validSSN);
		service.registrate(user);
	}

}
