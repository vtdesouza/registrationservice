package registrationservice.business.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import registrationservice.business.ExclusionService;
import registrationservice.business.impl.DefaultExclusionService;

public class DefaultExclusionServiceTest {

	private Map<String, String> excluded = new HashMap<>();
	private Map<String, String> allowed = new HashMap<>();
	
	private ExclusionService service;
	
	@Before
	public void setUp() throws Exception {
		service = new DefaultExclusionService();
		
		excluded.put("85385075", "1815-12-10");
		excluded.put("123456789", "1912-06-23");
		excluded.put("987654321", "1910-06-22");
		
		allowed.put("222222222", "1950-06-23");
		allowed.put("111111111", "1980-06-22");
		allowed.put("85385076", "1815-12-10");
		allowed.put("123456789", "1912-06-25");
	}

	@Test
	public void testValidate() {
		excluded.forEach((k, v) -> assertFalse("User <" + k + " | " + excluded.get(k) + "> should be excluded.", service.validate(excluded.get(k), k)));
		allowed.forEach((k, v) -> assertTrue("User <" + k + " | " + allowed.get(k) + "> should be allowed.", service.validate(allowed.get(k), k)));
	}

}
