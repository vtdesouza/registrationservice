package registrationservice.mock;

import registrationservice.business.impl.DefaultExclusionService;

public class ExclusionServiceMock extends DefaultExclusionService {

	public boolean valid = true; 
	
	@Override
	public boolean validate(String dateOfBirth, String ssn) {
		return valid;
	}

}
