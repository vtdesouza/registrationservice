**Object**

Small project to test a simple REST service implementation using Jetty.

**Curl example request**

curl -X POST http://localhost:8080/service/v1/register -d '{ "userName": "vitor", "password": "Asdj2ij", "dateOfBirth": "1910-06-22", "ssn": "123445" }' -H "Content-Type:application/json"